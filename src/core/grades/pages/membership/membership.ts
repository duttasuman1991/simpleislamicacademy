// (C) Copyright 2015 Moodle Pty Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import { Component, OnInit } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { CoreSitesProvider } from '@providers/sites';
import { DomSanitizer } from '@angular/platform-browser';


/**
 * Page that displays a course membership.
 */
@IonicPage({ segment: 'core-membership-course' })
@Component({
    selector: 'page-core-membership-course',
    templateUrl: 'membership.html',
})
export class CoreMembershipCoursePage implements OnInit {
    courseId: number;
    userId: number;
    url: any;
    admin: number;

    constructor(navParams: NavParams, sitesProvider: CoreSitesProvider, private dom: DomSanitizer) {
        this.courseId = navParams.get('courseId');
        this.userId = navParams.get('userId') || sitesProvider.getCurrentSiteUserId();
        this.admin = navParams.get('admin');
    }

    ngOnInit() {
        if (this.admin == 1) {
            var urlNew = `https://simplyislamacademy.co.uk/local/membership/appdashboard.php?uid=${this.userId}`;
        } else {
            var urlNew = `https://simplyislamacademy.co.uk/local/membership/appsubplans.php?uid=${this.userId}`;
        }

        this.url = this.dom.bypassSecurityTrustResourceUrl(urlNew);
    }
}
